package com.umich.android.statusupdater;

import java.io.IOException;
import java.net.MalformedURLException;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
//import android.util.Log;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ToggleButton;

public class UMichStatusUpdaterActivity extends Activity 
{
    FacebookClient fbClient = null;
    TwitterClient twClient = null;
	public static final String TAG = "twitter";
    String FILENAME = "AndroidSSO_data";
    private SharedPreferences mPrefs;
	private EditText mPostMessage;
	private Button postButton;
	private ToggleButton fbButton;
	private ToggleButton twButton;
	public static final int LOGINFB = Menu.FIRST;
	public static final int LOGINTW = Menu.FIRST + 1;
	
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        postButton = (Button)this.findViewById(R.id.button1);
        fbButton = (ToggleButton)this.findViewById(R.id.toggleButton1);
        twButton = (ToggleButton)this.findViewById(R.id.toggleButton01);
        mPostMessage = (EditText)this.findViewById(R.id.editText1);
        
        this.mPrefs = PreferenceManager.getDefaultSharedPreferences(this);
        
        //////////////////////////////////////////
        //////            FACEBOOK           /////
        //////////////////////////////////////////
        fbClient = new FacebookClient(mPrefs);
        
        //if existing session exists....
        if (mPrefs.getString("fb_access_token", "") != null &&  //null not ""?
	            mPrefs.getString("fb_access_token", "") != "")  //null not ""?
	            
	        {
	        	fbClient.authenticate(); //?
	        	fbClient.updateCredentials(mPrefs);
			    fbClient.isLoggedIn = true;
			  //  fbClient.isEnabled = true; //redundant?
                if (!fbClient.isEnabled)
                {
                	fbClient.flipEnabled();
            	    fbButton.toggle();
                }
	        }
        
        //////////////////////////////////////////
        //////            TWITTER            /////
        //////////////////////////////////////////
        twClient = new TwitterClient(mPrefs);
        
        if (mPrefs.getString("tw_access_token", "") != null &&
	            mPrefs.getString("tw_access_token", "") != "" &&
	            mPrefs.getString("tw_access_secret", "") != null &&
	            mPrefs.getString("tw_access_secret", "") != "")        
	        {

	            Log.v("Twitter", "Reusing Auth!");
                twClient.authenticate();
			    twClient.isLoggedIn = true;
			   // twClient.isEnabled = true; //redundant?
                if (!twClient.isEnabled)
                {
            	    twClient.flipEnabled();
            	    twButton.toggle();
                }
	        }
        
        //////////////////////////////////////////
        //////            BUTTONS            /////
        //////////////////////////////////////////
        this.postButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) 
            {
                String message = mPostMessage.getText().toString();
                
            	if (message.length() > 0)
            	{
            		//FACEBOOK POST
            		if (fbClient.isEnabled && fbClient.isLoggedIn)
            		{
                       fbClient.post(message);
            		}//end FACEBOOK
            		
            		//TWITTER POST
            		if (twClient.isEnabled && twClient.isLoggedIn)
            		{
            			twClient.post(message);
            		}
            	}//end empty message check
            }//end onClick
        }); //end ClickListener
        
        
        this.fbButton.setOnClickListener(new OnClickListener() {  //disable if not logged in?
            @Override
            public void onClick(View v) 
            {
            	if (fbClient.isLoggedIn)
            	{
            	    fbClient.flipEnabled();
            	}
            	else
            	{
            		fbButton.toggle();
            	}
            }//end onClick
        }); //end ClickListener
        
        
        this.twButton.setOnClickListener(new OnClickListener() {  //disable if not logged in?
            @Override
            public void onClick(View v) 
            {
            	if (twClient.isLoggedIn)
            	{
            	    twClient.flipEnabled();
            	}
            	else
            	{
            		twButton.toggle();
            	}
            }//end onClick
        }); //end ClickListener
        
    }//end OnCreate
    
    @Override
	public boolean onCreateOptionsMenu(Menu menu) {
		super.onCreateOptionsMenu(menu);
		menu.add(Menu.NONE, UMichStatusUpdaterActivity.LOGINFB, Menu.NONE, "Login Facebook");
		menu.add(Menu.NONE, UMichStatusUpdaterActivity.LOGINTW, Menu.NONE, "Login Twitter");
		return true;
	}
    
    @Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem loginFB = menu.findItem(Menu.FIRST);
		MenuItem loginTW = menu.findItem(Menu.FIRST + 1);
 
		if (fbClient.isLoggedIn) {
			loginFB.setTitle("Logout Facebook");
		} 
		else 
		{
			loginFB.setTitle("Login Facebook");
		}
		if (twClient.isLoggedIn) 
		{
			loginTW.setTitle("Logout Twitter");
		} 
		else 
		{
			loginTW.setTitle("Login Twitter");
		}
		return super.onPrepareOptionsMenu(menu);
	}
 
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int itemId = item.getItemId();
		switch (itemId) {
		case UMichStatusUpdaterActivity.LOGINFB:
			if (fbClient.isLoggedIn) 
			{
				try {
					fbClient.facebook.logout(this);
					fbClient.isLoggedIn = false;
	            	clearFBCredentials(); //could be only one level deep (inside FBClient.java?)
		            if (fbClient.isEnabled)
		            {
		            	fbClient.flipEnabled();
		            	fbButton.toggle();
		            }
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} 
			else 
			{
		        if (mPrefs.getString("fb_access_token", null) == null ||
		            mPrefs.getString("fb_access_token", null) == "")
		            
		        {
		        	fbClient.authorize(this);
		        	fbClient.updateCredentials(mPrefs); //remove?
		        	fbClient.authenticate(); //?
		        }
		        else
		        {
		        	fbClient.updateCredentials(mPrefs); //remove?
		            fbClient.authenticate();
			    }
				fbClient.isLoggedIn = true;
	            if (!fbClient.isEnabled)
	            {
	            	fbClient.flipEnabled();
	            	fbButton.toggle();
	            }
			}
			break;
		case UMichStatusUpdaterActivity.LOGINTW:
			if (twClient.isLoggedIn) 
			{
					//fbClient.facebook.logout(this);
					twClient.isLoggedIn = false;
	            	clearTWCredentials(); //could be only one level deep (inside FBClient.java?)
		            if (twClient.isEnabled)
		            {
		            	twClient.flipEnabled();
		            	twButton.toggle();
		            }
			} 
			else 
			{
		        if (mPrefs.getString("tw_access_token", "") == null ||
		            mPrefs.getString("tw_access_token", "") == "" ||
		            mPrefs.getString("tw_access_secret", "") == null ||
		            mPrefs.getString("tw_access_secret", "") == "")        
		        {
		            Log.v("Twitter", "Authorizing!");
		            Intent i = new Intent(getApplicationContext(), PrepareRequestTokenActivity.class);
		     		startActivity(i);
		        }
                else
                {
		            Log.v("Twitter", "Reusing Auth!");
	                twClient.authenticate();
                }
				twClient.isLoggedIn = true;
	            if (!twClient.isEnabled)
	            {
	            	twClient.flipEnabled();
	            	twButton.toggle();
	            }
			}
			break;
		default:
			return false;
		}
		return true;
	}
	
	private void clearFBCredentials() 
	{
		final Editor edit = mPrefs.edit();
		edit.remove("fb_access_token");
		edit.commit();
		fbClient.deleteCredentials();
	}
	
	private void clearTWCredentials() 
	{
		final Editor edit = mPrefs.edit();
		edit.remove("tw_access_token");
		edit.remove("tw_access_secret");
		edit.commit();
		twClient.deleteCredentials(); //remove?
	}
    
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) 
    {
        super.onActivityResult(requestCode, resultCode, data);
	    Log.v("Twitter", data.toString());
        fbClient.facebook.authorizeCallback(requestCode, resultCode, data);
    }
    
}