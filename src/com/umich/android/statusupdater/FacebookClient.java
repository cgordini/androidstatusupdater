package com.umich.android.statusupdater;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;

import com.facebook.android.*;
import com.facebook.android.Facebook.*;


//////////////////////////////////////////
//////            FACEBOOK           /////
//////////////////////////////////////////
public class FacebookClient
{
	private String APP_ID = "276913232351592";
    Facebook facebook = new Facebook(APP_ID);
    String FILENAME = "AndroidSSO_data";
    private SharedPreferences mPrefs;
    String access_token = null;
    
    boolean isEnabled = false;
    boolean isLoggedIn = false;

	private static final String[] PERMS = new String[] { "offline_access", "status_update", "publish_stream", "read_stream" };
	
    public FacebookClient(SharedPreferences inPrefs)
    {
        mPrefs = inPrefs;
    }//end CTOR
    
    public void authorize(Activity parent)
    {
    	facebook.authorize(parent, PERMS, new DialogListener() 
        {
            @Override
            public void onComplete(Bundle values) 
            {
                SharedPreferences.Editor editor = mPrefs.edit();
                editor.putString("fb_access_token", facebook.getAccessToken());
                editor.putLong("fb_access_expires", facebook.getAccessExpires());
                editor.commit();
            }

            @Override
            public void onFacebookError(FacebookError error) {}

            @Override
            public void onError(DialogError e) {}

            @Override
            public void onCancel() {}
        });
    }
    
    public void authenticate()
    {
    	long expires = 0;
    	//Get existing access_token (if any)
        if (mPrefs != null)
        {
            access_token = mPrefs.getString("fb_access_token", ""); //null?
            expires = mPrefs.getLong("fb_access_expires", 0);
        }
        if(access_token != null && access_token.length() > 0) 
        {
            facebook.setAccessToken(access_token);
            //Log.v("AT!", access_token);
        }

        if(expires != 0) {
            facebook.setAccessExpires(expires);
        }
    }
    
    public void deleteCredentials()
    {
		final Editor edit = mPrefs.edit();
		edit.remove("fb_access_token");
		edit.commit();
    }
    
    public void updateCredentials(SharedPreferences inPreferences) //works?
    {
		inPreferences = mPrefs;
    }
    
    public void flipEnabled()
    {
    	this.isEnabled = !this.isEnabled;
    }
    
    public void post(String status)
    {
    	 try 
         {
             String response = facebook.request("me");
             Bundle parameters = new Bundle();
             parameters.putString("message", status); //putString?
             parameters.putString("description", "UMichStatusUpdater"); //putString?
             response = facebook.request("me/feed", parameters, "POST");
             if (response == null || response.equals("") || 
                 response.equals("false")) 
             {
                 Log.v("Error", "Blank response");
             }
             else
             {
                 Log.v("FB Success!", response);
             }
         } 
         catch(Exception e) 
         {
             e.printStackTrace();
         }//end catch
    }
    

}//end class