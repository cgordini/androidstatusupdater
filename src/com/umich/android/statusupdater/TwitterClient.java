package com.umich.android.statusupdater;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.http.AccessToken;
//import android.app.Activity;
//import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
//import android.net.Uri;
//import android.os.Bundle;
import android.util.Log;

//////////////////////////////////////////
//////            TWITTER            /////
//////////////////////////////////////////
public class TwitterClient
{
	//private String CONSUMER_KEY = "u6fPJMkJDrxgZGmglu75gg";
	//private String CONSUMER_SECRET = "e8918ZfSopNEkclo0OtKMKdh7qWBBZ5qWFtEcFAk0E";
	
	private String CONSUMER_KEY = "mdJYB34VEEs9kfrmN0WQ";
	private String CONSUMER_SECRET = "1RxAjMPBa7QX84c3NPxEj45k65Pz87EQknHFhhU";
	public static final String TAG = "twitter";
	private Twitter twitter = null;
	public static final String CALLBACK_URI = "twitter://callback";
	public static final String CANCEL_URI = "twitter://cancel";
	public static String ACCESS_TOKEN = "access_token";
	public static String SECRET_TOKEN = "secret_token";

	public static final String REQUEST = "request";
	public static final String AUTHORIZE = "authorize";

	protected static String REQUEST_ENDPOINT = "https://api.twitter.com/1";
	
	protected static String OAUTH_REQUEST_TOKEN = "https://api.twitter.com/oauth/request_token";
	protected static String OAUTH_ACCESS_TOKEN = "https://api.twitter.com/oauth/access_token";
	protected static String OAUTH_AUTHORIZE = "https://api.twitter.com/oauth/authorize";
	//private final String CALLBACKURL = "UMichStatusUpdater://UMichStatusUpdater"; //"UMichStatusUpdater://main";

	private SharedPreferences mPrefs;
	
    boolean isEnabled = false;
    boolean isLoggedIn = false;
	
    public TwitterClient(SharedPreferences inPrefs)
    {
	    twitter = new TwitterFactory().getInstance();
	    twitter.setOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
        mPrefs = inPrefs;
    }//end CTOR    
    
    @SuppressWarnings("deprecation")
	public void authenticate()
    {
    	String token = mPrefs.getString("tw_access_token", "");
    	String secret = mPrefs.getString("tw_access_secret", "");

    	AccessToken a = new AccessToken(token,secret);
    	twitter = new TwitterFactory().getInstance();
    	twitter.setOAuthConsumer(CONSUMER_KEY, CONSUMER_SECRET);
    	twitter.setOAuthAccessToken(a);
    	
    }
    
    public void flipEnabled()
    {
    	this.isEnabled = !this.isEnabled;
    }
    
    public void deleteCredentials()
    {
		final Editor edit = mPrefs.edit();
		edit.remove("tw_access_token");
		edit.remove("tw_access_secret");
		edit.commit();
    }
    
    public void post(String status)
    {
	    try 
	    {
	        twitter.updateStatus(status);
            Log.v("Twitter Success!", status);

	    } 
	    catch (TwitterException e) 
	    {
	        Log.e(TAG, e.toString());
	    }
    }

}//end class*/